#pragma once

namespace lab6
{
	template<class T, size_t N>
	class FixedVector
	{
	public:
		FixedVector();
		~FixedVector();
		const T& Get(unsigned int index) const;
		T& operator[](unsigned int index);
		int GetIndex(const T& t) const;
		size_t GetSize() const;
		size_t GetCapacity() const;
		bool Add(const T& t);
		bool Remove(const T& t);

	private:
		size_t mSize;
		T mArray[N];
	};
	template<class T, size_t N>
	inline FixedVector<T, N>::FixedVector()
		: mSize(0)
	{
	}
	template<class T, size_t N>
	inline FixedVector<T, N>::~FixedVector()
	{
	}
	template<class T, size_t N>
	inline const T& FixedVector<T, N>::Get(unsigned int index) const
	{
		return mArray[index];
	}
	template<class T, size_t N>
	inline T& FixedVector<T, N>::operator[](unsigned int index)
	{
		return mArray[index];
	}
	template<class T, size_t N>
	inline int FixedVector<T, N>::GetIndex(const T& t) const
	{
		for (size_t i = 0; i < mSize; i++)
		{
			if (mArray[i] == t)
			{
				return i;
			}
		}
		return -1;
	}
	template<class T, size_t N>
	inline size_t FixedVector<T, N>::GetSize() const
	{
		return mSize;
	}
	template<class T, size_t N>
	inline size_t FixedVector<T, N>::GetCapacity() const
	{
		return N;
	}
	template<class T, size_t N>
	inline bool FixedVector<T, N>::Add(const T& t)
	{
		if (mSize >= N)
		{
			return false;
		}
		else
		{
			mArray[mSize++] = t;
			return true;
		}
	}
	template<class T, size_t N>
	inline bool FixedVector<T, N>::Remove(const T& t)
	{
		for (size_t i = 0; i < mSize; i++)
		{
			if (mArray[i] == t)
			{
				for (size_t j = i + 1; j < mSize; j++)
				{
					mArray[i] = mArray[j];
					i++;
				}
				mArray[mSize--];
				return true;
			}
		}
		return false;
	}
}
