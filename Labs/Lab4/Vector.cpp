#include "Vector.h"

namespace lab4
{
	Vector::Vector(float x, float y, float z) : x(x), y(y), z(z)
	{
	}

	Vector::~Vector()
	{
	}

	Vector Vector::operator+(const Vector& other) const
	{
		Vector vec = Vector(GetX(), GetY(), GetZ());
		vec.x += other.GetX();
		vec.y += other.GetY();
		vec.z += other.GetZ();
		return vec;
	}

	Vector Vector::operator-(const Vector& other) const
	{
		Vector vec = Vector(GetX(), GetY(), GetZ());
		vec.x -= other.GetX();
		vec.y -= other.GetY();
		vec.z -= other.GetZ();
		return vec;
	}

	float Vector::operator*(const Vector& other) const
	{
		Vector vec = Vector(GetX(), GetY(), GetZ());
		return ((vec.GetX() * other.GetX()) + (vec.GetY() * other.GetY()) + (vec.GetZ() * other.GetZ()));
	}

	Vector Vector::operator*(float operand) const
	{
		Vector vec = Vector(GetX(), GetY(), GetZ());
		vec.x *= operand;
		vec.y *= operand;
		vec.z *= operand;
		return vec;
	}

	float Vector::operator[](unsigned int index) const
	{
		if (index <= 0)
		{
			return GetX();
		}
		if (index == 1)
		{
			return GetY();
		}
		if (index >= 2)
		{
			return GetZ();
		}
		return 0;
	}

	float Vector::GetX() const
	{
		return x; // return x component
	}
	float Vector::GetY() const
	{
		return y; // return y component
	}
	float Vector::GetZ() const
	{
		return z; // return z component
	}

	Vector operator*(float operand, const Vector& v)
	{
		Vector vec = Vector(v.GetX(), v.GetY(), v.GetZ());
		vec.x *= operand;
		vec.y *= operand;
		vec.z *= operand;
		return vec;
	}
}
