#include "Lab2.h"

#include <string>
#include <iomanip>

using namespace std;

namespace lab2
{
	void PrintIntegers(std::istream& in, std::ostream& out)
	{
		int number;
		int octFill = 12;
		int decFill = 11;
		int hexFill = 9;
		out << setfill(' ') << right << setw(octFill) << "oct";
		out << setfill(' ') << right << setw(decFill) << "dec";
		out << setfill(' ') << right << setw(hexFill) << "hex" << endl;
		out << "------------ ---------- --------" << endl;
		while (!in.eof())
		{
			if (in.fail())
			{
				in.clear();
				in.ignore();
			}
			if (in >> number)
			{
				out << oct << setfill(' ') << setw(octFill) << number;
				out << dec << setfill(' ') << setw(decFill) << number;
				out << hex << setfill(' ') << setw(hexFill) << uppercase << number << endl;
			}
		}


	}

	void PrintMaxFloat(std::istream& in, std::ostream& out)
	{
		float number;
		char sign;
		float max = 0;

		while (!in.eof())
		{
			if (in.fail())
			{
				in.clear();
				in.ignore();
			}
			if (in >> number)
			{
				if (number >= 0)
				{
					sign = '+';
				}
				else
				{
					sign = '-';
				}
				if (number > max)
				{
					max = number;
				}
				out << setfill(' ') << right << setw(6) << sign;
				out << setfill(' ') << right << setw(14) << setprecision(3) << fixed << abs(number) << endl;
			}
		}
		if (max >= 0)
		{
			sign = '+';
		}
		else
		{
			sign = '-';
		}
		out << setfill(' ') << left << setw(5) << "max:";
		out << right << setw(1) << sign;
		out << setfill(' ') << right << setw(14) << max << endl;

	}

}
