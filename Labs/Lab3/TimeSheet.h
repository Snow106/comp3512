#pragma once

#include <string>

using namespace std;
namespace lab3
{
	class TimeSheet
	{
	public:
		TimeSheet(const char* employeeName, int maxEntries);
		// Write a copy constructor and a destructor here.
		virtual ~TimeSheet();
		TimeSheet(const TimeSheet& obj);

		void AddTime(float hours);
		float GetTotalTime() const;
		float GetAverageTime() const;
		const std::string& GetName() const;

	private:
		float totalTime = 0;
		string name = "";
		int maxTime = 10;
		int entries = 0;
		int maxEntries = 0;

	};
}
