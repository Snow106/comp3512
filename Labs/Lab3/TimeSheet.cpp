#include "Timesheet.h"

namespace lab3
{
	TimeSheet::TimeSheet(const char* name, int maxEntries)
		: name(name)
		, entries(0)
		, maxEntries(maxEntries)
	{
	}
	TimeSheet::TimeSheet(const TimeSheet& obj)
		: name(obj.name)
		, totalTime(obj.totalTime)
		, maxTime(obj.maxTime)
		, entries(obj.entries)
		, maxEntries(obj.maxEntries)
	{
	}

	TimeSheet:: ~TimeSheet()
	{
	}

	void TimeSheet::AddTime(float timeInHours)
	{
		if ((timeInHours <= 10 && timeInHours > 0) && (entries < maxEntries))
		{
			totalTime += timeInHours;
			entries++;
		}
	}

	float TimeSheet::GetTotalTime() const
	{
		return totalTime;
	}

	float TimeSheet::GetAverageTime() const
	{
		if (totalTime <= 0)
		{
			return 0;
		}
		return totalTime / entries;
	}

	const std::string& TimeSheet::GetName() const
	{
		return name;
	}
}