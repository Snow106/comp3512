#include <iostream>

#include "Blizzard.h"
#include "Candy.h"
#include "Donut.h"
#include "IceCream.h"
#include "Item.h"
#include "ShoppingCart.h"

using namespace assignment1;

// Cat.h
class Cat
{
public:
	Cat();
	int GetCount() const;
private:
	static int mCount;
};

int Cat::mCount = 11;
Cat::Cat()
{
	mCount++;
}

int Cat::GetCount() const
{
	return mCount / 3;
}

int main()
{
	Cat* cat = new Cat();
	Cat* cat1 = new Cat();
	std::cout << cat1->GetCount() << std::endl;
	std::cout << cat1->GetCount() << std::endl;
}
