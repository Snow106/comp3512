#include "Donut.h"

namespace assignment1
{
	Donut::Donut(const char* name, unsigned int count)
		: Item(name)
		, count(count)
	{
	}

	Donut::~Donut()
	{
	}
	unsigned int Donut::GetCost() const
	{
		float total = price / 12 * count;
		return (int)(total + 0.5); // return cost here
	}
}
