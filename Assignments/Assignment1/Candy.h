#pragma once

#include "Item.h"

namespace assignment1
{
	class Candy : public Item
	{
	public:
		Candy(const char* name, float weight);
		virtual ~Candy();
		unsigned int GetCost() const;

	private:
		double weight;
		int price = 68;
		// private variables here
	};
}
