#include "Candy.h"

namespace assignment1
{
	Candy::Candy(const char* name, float weight)
		: Item(name)
		, weight(weight)
	{
	}

	Candy::~Candy()
	{
	}

	unsigned int Candy::GetCost() const
	{
		return (int)(price * weight + 0.5); // return cost here
	}
}
