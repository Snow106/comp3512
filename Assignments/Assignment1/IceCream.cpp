#include "IceCream.h"
#include "Cone.h"

namespace assignment1
{
	IceCream::IceCream(const char* name, unsigned int scoops)
		: Item(name)
		, mCone(NULL)
		, scoops(scoops)
	{
	}

	IceCream::IceCream(const IceCream& other)
		: Item(other)
		, scoops(other.scoops)
		, price(other.price)
	{
		if (other.mCone == NULL)
		{
			mCone = NULL;
		}
		else
		{
			mCone = new Cone();
		}
	}

	IceCream::~IceCream()
	{
		delete mCone;
	}

	unsigned int IceCream::GetCost() const
	{
		int cone = 0;
		if (mCone != NULL)
		{
			cone += mCone->GetCost();
		}
		return (int)(price * scoops + cone + 0.5); // return cost here
	}

	void IceCream::AddCone()
	{
		if (mCone == NULL)
		{
			mCone = new Cone();
		}
	}
}