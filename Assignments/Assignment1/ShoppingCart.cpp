#include "ShoppingCart.h"
#include "Item.h"

namespace assignment1
{
	ShoppingCart::ShoppingCart()
	{
		itemCount = 0;
		cart = new const Item*[CART_SIZE];
		for (int i = 0; i < sizeof(cart); i++)
		{
			cart[i] = NULL;
		}
	}

	ShoppingCart::~ShoppingCart()
	{
		for (unsigned int i = 0; i <	; i++)
		{
			delete cart[i];
		}
		delete[] cart;
	}

	bool ShoppingCart::AddItem(const Item* item)
	{
		if (item != NULL && itemCount < 10)
		{
			cart[itemCount] = item;
			itemCount++;
			return true;
		}
		else
		{
			return false; // return true if success. Else return false
		}
	}

	bool ShoppingCart::RemoveItem(unsigned int index)
	{
		if (index >= itemCount)
		{
			return false;
		}
		else
		{
			delete cart[index];
			if (index != itemCount - 1)
			{
				for (unsigned int i = index; i < itemCount - 1; i++)
				{
					cart[i] = cart[i + 1];
				}
			}
			cart[itemCount] = NULL;
			itemCount--;
			return true;
		}
	}

	const Item* ShoppingCart::GetItem(unsigned int index) const
	{
		if (index >= itemCount)
		{
			return NULL;
		}
		else
		{
			return cart[index];
		}
	}

	const Item* ShoppingCart::operator[](unsigned int index) const
	{
		if (index > itemCount)
		{
			return NULL;
		}
		else
		{
			return cart[index];
		}
	}

	float ShoppingCart::GetTotal() const
	{
		float sum = 0;

		for (unsigned int i = 0; (i < itemCount); i++)
		{
			sum += cart[i]->GetCost();
		}

		return sum / 100; // return total cost in dollars
	}
}