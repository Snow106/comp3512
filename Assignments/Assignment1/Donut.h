#pragma once

#include "Item.h"

namespace assignment1
{
	class Donut : public Item
	{
	public:
		Donut(const char* name, unsigned int count);
		virtual ~Donut();
		unsigned int GetCost() const;

	private:
		// private variables here
		int count;
		float price = 899;
	};
}
